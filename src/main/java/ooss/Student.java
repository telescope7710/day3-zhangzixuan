package ooss;

public class Student extends Person {

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        if(this.klass == null)
            return "My name is " + this.name + ". I am " + this.age + " years old. I am a student.";
        if(this.klass.isLeader(this)) {
            return "My name is " + this.name + ". I am " + this.age + " years old. I am a student. I am the leader of class "+this.klass.hashCode()+".";
        }else{
            return "My name is " + this.name + ". I am " + this.age + " years old. I am a student. I am in class "+this.klass.hashCode()+".";
        }
    }

    public void join(Klass klass){
        this.klass = klass;
    }

    public boolean isIn(Klass klass){
        if(this.klass != null){
            return this.klass.equals(klass);
        }else{
            return false;
        }
    }
}
