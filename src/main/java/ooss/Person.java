package ooss;

public class Person {
    protected int id;
    protected String name;
    protected int age;

    public Person(int id, String name, int age) {
        this.age = age;
        this.name = name;
        this.id = id;
    }

    public String introduce(){
        return "My name is " + this.name + ". I am " + this.age + " years old.";
    }
}
