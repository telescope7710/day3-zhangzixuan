package ooss;

import java.util.*;
import java.util.stream.Collectors;

public class Teacher extends Person {
    List<Klass> klassList = new ArrayList<>();

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String introduction = "My name is " + this.name + ". I am " + this.age + " years old. I am a teacher.";
        if(klassList.isEmpty()) {
            return introduction;
        }else{
                String classLine= klassList.stream().map(Klass::hashCode).collect(Collectors.toList()).toString();
                return introduction + " I teach Class " + classLine.substring(1, classLine.length() - 1) + ".";
        }
    }

    public void assignTo(Klass klass){
        klassList.add(klass);
    }

    public boolean belongsTo(Klass klass){
        return klassList.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return !klassList.stream().filter(student::isIn).collect(Collectors.toList()).isEmpty();
    }
}
