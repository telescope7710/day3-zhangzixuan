package ooss;

public class Klass {
    private int number;
    private Student leader;
    private Person attach;

    public Klass(int number) {
        this.number = number;
    }

    @Override
    public int hashCode() {
        return number;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Klass)){
            return false;
        }
        return this.hashCode()== obj.hashCode();
    }

    public void assignLeader(Student student) {
        if(student.isIn(this)) {
            this.leader = student;
        }else {
            System.out.println("It is not one of us.");
        }
        System.out.println("I am " + this.attach.name + ", " + this.attach.getClass().toString().toLowerCase().substring(11) + " of Class 2. I know " + this.leader.name + " become Leader.");
    }

    public boolean isLeader(Student student) {
        return this.leader.equals(student);
    }

    public void attach(Person person) {
        this.attach = person;
    }
}
